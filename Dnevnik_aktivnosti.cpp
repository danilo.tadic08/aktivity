#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>

using namespace std;

int main(){
	
	//Za korisnicko ime program sve prihvata, ali neophodan je unos na latinicnom pismu bez upotrebe dijakriticnih znakova.
	//Lozinka za ulaz u program je "54132".
	
	string username; //Korisnicko ime.
	string password = "0101"; //Lozinka.
	string password_input;
	string dateinput; //Datum za prijavljivanje/odjavljivanje.
	string timeinput; //Vreme za prijavljivanje/odjavljivanje.
	string pl; //Sifra prijavljivanja.
	string w; //Posao koji se obavlja u aktivnosti. Za razmak se koristi "_".
	string wth; //Trajanje posla (sati).
	string wtm; //Trajanje posla (minuti).
	string date; //Danasnji datum.
	string dateup; //Poslednje azuriranje.
	string date1; //Rezervni datum.
	string line; //fstream.
	string nu; //novi unos.
	int x; //Prvi meni odabir.
	int bp = 3; //Broj preostalih pokusaja za prijavljivanje.
	
	ifstream datum;
	datum.open("D:/4. Baza podataka/6. Server/! Podaci/Datum/danasnji datum.txt");
	getline(datum, line);
	date = line;
	datum.close();
	
	ifstream datum1;
	datum1.open("D:/4. Baza podataka/6. Server/! Podaci/Datum/danasnji datum.txt");
	getline(datum1, line);
	date1 = line;
	datum1.close();
	
	ifstream update;
	update.open("D:/4. Baza podataka/6. Server/! Podaci/Datum/update/dnevnik aktivnosti.txt");
	getline(update, line);
	dateup = line;
	update.close();
	
	cout<< "/////////////////////////////////////////////////////////////////////" << endl;
	cout<< "/		Zvanicna aplikacija Jedinice Soko	            "                 "/" << endl;
	cout<< "/////////////////////////////////////////////////////////////////////" << endl;
	cout<< " - AKTIVNOST NA POSLU." << endl;
	cout<< endl;
	cout<< " PRIJAVLJIVANJE NA SISTEM:" <<endl << endl;
	cout<< " Korisnicko ime: ";
	cin>> username; //Unos korisnickog imena.

pass_login:
	char pass;
    cout << " Lozinka: ";
    while((pass = _getch()) != 13){
    	string str;
    	
        password_input += pass;
        cout << '*';
    }
	cout<< endl;
	if(password_input == password){
		cout<< "----------------------------------------" << endl;
		cout<< " Prijavili ste se kao >>" << username << "<<." << endl;
		cout<< endl;
		
		cout<< " Danasnji datum: " << date << endl;
		cout<< " Poslednje azuriranje: " << dateup << endl;	
		
		cout<< endl;		
	pitanje1:	cout<< " Odaberite opciju:" << endl;
		cout<< " 	1. Prijavite se;" << endl;
		cout<< " 	2. Odjavite se;" << endl;
		cout<< " Vas odabir: ";
		cin>> x;
		
		if(x == 1){
			cout<< endl;
			cout<< " Prijavljivanje:" << endl;
			cout<< endl;
			cout<< " Unesite sifru prijavljivanja koja sledi (poslednja sifra: ";
			ifstream file0;
			file0.open("D:/4. Baza podataka/6. Server/! Sever log/2.-Dnevnik-aktivnosti/Aktivnost - poslednje prijavljivanje.txt");
			getline(file0, line);
			cout<< line;
			file0.close();
			cout<< "): ";
			cin>> pl;
			
			ofstream file01;
			file01.open("D:/4. Baza podataka/6. Server/! Sever log/2.-Dnevnik-aktivnosti/Aktivnost - poslednje prijavljivanje.txt");
			file01<< pl;
			file01.close();
				
			cout<< endl;
			cout<< " Unesite vreme kada ste se prijavili na posao (hh:mm):" << endl << " 	";
			cin>> timeinput;
			cout<< endl;
			
			cout<< " Unesite posao radjen za vreme ove aktivnosti:" << endl;
			cout<< "	";
			getline(cin, w);
			
			ofstream file02;
			file02.open("D:/4. Baza podataka/6. Server/! Sever log/2.-Dnevnik-aktivnosti/Prijava/Aktivnost - prijava.txt", fstream::app);
			getline(cin, w);
			file02<< w << " - " << username << ", " << date1 << " " << timeinput << " (" << pl << ")" << endl;
			file02<< endl;
			file02.close();
			//Unos prijave.
			
			cout<< endl;
			cout<< " Uspesno ste se prijavili na posao." << endl;
			
			cout<< endl;
			cout<< " Da li zelite jos unosa podataka (Da/Ne): ";
			cin>> nu;
			
			if(nu == "D" || nu == "DA" || nu == "d" || nu == "da" || nu == "Da"){
				cout<< endl;
				goto pitanje1;
			}				
			else{
				cout<< endl;
				cout<< "Kraj aplikacije. Zatvaranje...";
				cout<< endl;
				system("pause");
				return 0;
			}
		}
		
		if(x == 2){
			cout<< endl;
			cout<< " Odjavljivanje:" << endl;
			cout<< endl;
			cout<< " Unesite sifru prijavljivanja koju zelite da ukinete (zadnje koriscena: "; 
			
			ifstream file5;
			file5.open("D:/4. Baza podataka/6. Server/! Sever log/2.-Dnevnik-aktivnosti/Aktivnost - poslednje prijavljivanje.txt");
			getline(file5, line);
			cout<< line;
			file5.close();
			
			cout<< "): ";
			cin>> pl;
			
			cout << endl;
			cout<< " Unesite vreme odjavljivanja (hh:mm):" << endl;
			cout<< " 	";
			cin>> timeinput;
			
			cout<< endl;
			cout<< " Unesite vreme trajanja aktivnosti (h m): ";
			cin>> wth >> wtm;
			
			ofstream file03;
			file03.open("D:/4. Baza podataka/6. Server/! Sever log/2.-Dnevnik-aktivnosti/Odjava/Aktivnost - odjava.txt", fstream::app);
			file03 << "Odjavljena je aktivnost sa sifrom " << pl << ", " << date1 << " u " << timeinput << " i trajala je " << wth << " h i " << wtm << " min."; 
			file03 << " Aktivnost je otkazao korisnik sa korisnickim imenom >>" << username << "<<." << endl;
			file03 << endl;
			file03.close();
			//Unos odjave.
			
			cout<< endl;
			cout<< " Uspesno ste odjavili aktivnost." << endl;
			
			cout<< endl;
			cout<< " Da li zelite jos unosa podataka (Da/Ne): ";
			cin>> nu;
			
			if(nu == "D" || nu == "DA" || nu == "d" || nu == "da" || nu == "Da"){
				cout<< endl;
				goto pitanje1;
			}
			else{
				cout<< endl;
				cout<< "Kraj aplikacije. Zatvaranje...";
				cout<< endl;
				system("pause");
				return 0;
			}				
		}
		
		if(x == 0){
			cout<< endl;
			cout<< " Niste nista odabrali." << endl;
			
			cout<< endl;
			cout<< "Kraj aplikacije. Zatvaranje...";
			cout<< endl;
			system("pause");
			return 0;			
		}
	}

						
	if(password != password_input){
		for(int i = 1; i < bp; i++){
			bp--;
			cout<< endl << " ------------------------------------" << endl;
			cout<< " Netacna lozinka. Pokusajte ponovo..." << endl;
		
			ofstream file04;
			file04.open("D:/4. Baza podataka/6. Server/! Sever log/2.-Dnevnik-aktivnosti/Aktivnost - netacna prijavljivanja.txt", fstream::app);
			file04 << "*Pokusaj prijavljivanja:" << endl;
			file04 << "  " << username << ", " << date << " (" << password << ")" << endl;
			file04 << endl;
			file04.close();
				
			if(bp == 2){
				cout<< " Ostalo Vam je jos " << bp << " pokusaja." << endl;
				password_input.clear();	
				goto pass_login;			
			}
			if(bp == 1){
				cout<< " Ostao Vam je jos " << bp << " pokusaj." << endl;
				password_input.clear();				
				goto pass_login;
			}
		}
	}
	
	return 0;
}
